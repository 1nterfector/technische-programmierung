#include <stdio.h>
#include <math.h>

#define LENGTH_NOTEN 40

typedef unsigned char bool; // Bool definieren
#define true 1
#define false 0

// Methoden Koepfe
bool isValidInput(double);
void sortDoubleArray(double *a, int n);
int compDouble(const void *, const void *);


int notenMain() {	
	double noten[LENGTH_NOTEN];
	double avg, median, stdDeviation;
	int i, anzNoten;

	printf("Max %i Noten eingeben (-1 zum abbrechen)\n", LENGTH_NOTEN);
	for (anzNoten = 0; anzNoten < LENGTH_NOTEN; anzNoten++)
	{
		// noten[anzNoten] = 0;  vllt ?
		printf("%i: ", anzNoten+1);
		scanf_s("%lf", &noten[anzNoten]);
		
		// Abbruch Bedingung
		if (noten[anzNoten] == -1) break;
		
		// Überprüfen ob Note gueltig ist
		if (!isValidInput(noten[anzNoten])) {
			printf("Keine g\x81ltige Note, bitte wiederholen\n");
			anzNoten--;
			continue;
		}
	}

	// Noten sortiert ausgeben
	system("@cls||clear");
	sortDoubleArray(noten, anzNoten);
	for ( i = 0; i < anzNoten; i++)
	{
		printf("%.1f\n", noten[i]);
	}
	printf("\n");


	// Durchschnitt berechnen
	avg = 0;
	for (i = 0; i < anzNoten; i++)
	{
		avg += noten[i];
	}
	avg /= anzNoten;

	// Median "berechnen"
	if (anzNoten % 2 == 1) median = noten[(int)(anzNoten/2)];
	else median = (noten[anzNoten / 2] + noten[anzNoten / 2 - 1]) / 2;

	// Standardabweichung berechnen
	stdDeviation = 0;
	for (i = 0; i < anzNoten; i++)
	{
		stdDeviation += (noten[i] - avg)*(noten[i] - avg);
	}
	stdDeviation = sqrt(stdDeviation/anzNoten);


	printf("Avg: %f, Median: %f, Standardabweichung: %f\n", avg, median, stdDeviation);

	// Ende
	printf("\n\n\n\n\n\n\n");
	return 0;
}




bool isValidInput(double n) {
	for (int i = 1; i <= 3; i++)
	{
		if (n == i || n == i + 0.3 || n == i + 0.7) return true;
	}
	if (n == 4 || n == 5) return true;
	return false; 
}

void sortDoubleArray(double * a, int n) {
	qsort(a, n, sizeof(*a), compDouble);
}

int compDouble(const void * elem1, const void * elem2)
{
	double f = *((double*)elem1);
	double s = *((double*)elem2);
	if (f > s) return  1;
	if (f < s) return -1;
	return 0;
}