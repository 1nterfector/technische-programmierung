#define NUM_S 3
#include <stdio.h>

int dreieckMain() {
	const double tol = 0.00001;
	double s[NUM_S];
	int i, maxIndex;
	double sum;
	

	printf("Bitte die drei Dreiecksseitenl\x84ngen eingeben\n");

	for (i = 0; i < NUM_S; i++) {
		printf("Seite %i: ", i+1);
		scanf_s("%lf", &s[i]);
		if (s[i] <= 0) {
			printf("Bitte positiven Wert eingeben!\n");
			i--;
			continue;
		}
	}

	// Eingegeben Zahlen ausgeben
	system("@cls||clear");
	printf("1: %f", s[0]);
	for (i = 1; i < NUM_S; i++)
	{
		printf(", %i: %f", i+1, s[i]);
	}
	printf("\n");


	// Maximum suchen und Summe bilden
	sum = s[0];
	maxIndex = 0;
	for (i = 1; i < NUM_S; i++)
	{
		sum += s[i];
		if (s[i] > s[maxIndex]) maxIndex = i;
	}

	// Pruefen der Dreiecksungleichung
	if (sum - s[maxIndex] < s[maxIndex]) {
		printf("Kein zusammenh\x84ngendes Polygon\n");
	} else if(NUM_S == 3){
		// Pruefen ob rechter Winkel vorhanden ist
		sum = 0;
		for (i = 0; i < NUM_S; i++)
		{
			if (i == maxIndex) continue;
			sum += s[i] * s[i];
		}
		
		if ((s[maxIndex] * s[maxIndex] - sum) * (s[maxIndex] * s[maxIndex] - sum) < tol) printf("Dreieck besitzt einen rechten Winkel\n");
	}

	printf("\n\n\n\n\n");
	return 0;
}